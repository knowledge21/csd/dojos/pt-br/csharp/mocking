using System.Collections.Generic;
using Moq;
using NUnit.Framework;

namespace com.br.k21.SistemaVendas.Testes
{
    public class TestesCalculadoraRoyalties
    {
        private List<Venda> listaDeVendas;
        private Mock<VendaRepository> minhaMarionete;

        [SetUp]
        public void construirMarionetes()
        {
            listaDeVendas = new List<Venda>();
            minhaMarionete = new Mock<VendaRepository>();
        }

        [Test]
        public void Mes_01_de_2019_com_venda_de_1000_retorna_royalties_de_190()
        {
            int mes = 01;
            int ano = 2019;
            double royaltiesEsperados = 190;

            listaDeVendas.Add(new Venda(1, 1000));

            minhaMarionete.Setup(m => m.ObterVendasPorMesEAno(ano, mes)).Returns(listaDeVendas);

            double royaltiesCalculados = new CalculadoraRoyalties(minhaMarionete.Object).Calcular(mes, ano);

            Assert.AreEqual(royaltiesEsperados, royaltiesCalculados);
        }

        [Test]
        public void Mes_02_de_2019_com_vendas_de_100k_e_de_55_59_retorna_royalties_de_18810_56()
        {
            int mes = 02;
            int ano = 2019;
            double royaltiesEsperados = 18810.56;

            listaDeVendas.Add(new Venda(1, 100000));
            listaDeVendas.Add(new Venda(2, 55.59));

            minhaMarionete.Setup(m => m.ObterVendasPorMesEAno(ano, mes)).Returns(listaDeVendas);

            double royaltiesCalculados = new CalculadoraRoyalties(minhaMarionete.Object).Calcular(mes, ano);

            Assert.AreEqual(royaltiesEsperados, royaltiesCalculados);
        }

        [Test]
        public void Mes_12_de_2022_sem_vendas_retorna_royalties_de_0()
        {
            int mes = 12;
            int ano = 2022;
            double royaltiesEsperados = 0;

            double royaltiesCalculados = new CalculadoraRoyalties().Calcular(mes, ano);

            Assert.AreEqual(royaltiesEsperados, royaltiesCalculados);
        }
    }
}