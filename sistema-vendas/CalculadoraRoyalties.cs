﻿using System;
using System.Collections.Generic;

namespace com.br.k21.SistemaVendas
{
    public class CalculadoraRoyalties
    {
        private VendaRepository Repository;

        public CalculadoraRoyalties()
        {
            this.Repository = new VendaRepository();
        }

        public CalculadoraRoyalties(VendaRepository repository)
        {
            this.Repository = repository;
        }

        public double Calcular(int mes, int ano)
        {
            List<Venda> vendas = Repository.ObterVendasPorMesEAno(ano, mes);

            double faturamento = 0;
            double comissoes = 0;

            if (vendas != null)
            {
                foreach (Venda venda in vendas)
                {
                    faturamento += venda.Valor;
                    comissoes += new CalculadoraComissao().Calcular(venda.Valor);
                }
            }

            double royalties = (faturamento - comissoes) * 0.2;

            return Math.Floor(royalties * 100) / 100;
        }
    }
}
